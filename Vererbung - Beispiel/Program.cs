﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VererbungBeispiel
{
    class Program
    {
        static void Main(string[] args)
        {
            KlasseMitKonstruktor klasse = new KlasseMitKonstruktor();

            Auto auto = new Auto();
            Motorrad motorrad = new Motorrad();

            auto.GibGas();
            motorrad.GibGas();

            Fahrzeug fahrzeug = null;
            fahrzeug = auto;
            fahrzeug.GibGas();            
            //((Auto)fahrzeug).blinken();

            //auto.blinken();
            //fahrzeug.blinken();

            motorrad.GibGas();
            ((Fahrzeug)motorrad).GibGas();

            auto.GibGas();
            ((Fahrzeug)auto).GibGas();

            Schneemobil schneemobil = new Schneemobil();

            List<IFahrzeug> liste = new List<IFahrzeug>();
            liste.Add(auto);
            liste.Add(motorrad);
            liste.Add(schneemobil);

            schneemobil?.Auto?.GibGas();

            if(schneemobil != null && schneemobil.Auto != null)
            {
                schneemobil.Auto.GibGas();
            }

            int? i = null;

            int j = i ?? 2;

            j = i != null ? (int)i : 2;

            if(i != null)
            {
                j = (int)i;
            }

            MethodenBeispiel mb = new MethodenBeispiel();

            mb.Inkrement(ref j);
        }
    }
}

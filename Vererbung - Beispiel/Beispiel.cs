﻿namespace VererbungBeispiel
{
    // Einfachste Klasse
    class Beispiel
    {
    }

    // Nur in der eigenen Assembly(DLL / EXE) sichtbar
    internal class interneKlasse
    {
    }

    // Für alle sichtbar
    public class oeffentlicheKlasse
    {
    }

    // Vor Vererben geschützt. Von dieser Klasse kann nicht geerbt werden.
    public sealed class geschuetzeKlasse
    {
    }

    public abstract class abstrakteKlasse
    {
    }

    public abstract class MethodenZugriff
    {
        // diese Methode nach aussen sichtbar
        public void sichtbareMethode()
        {
        }

        // Nur innerhalb dieser Klasse sichtbar
        private void privateMethode()
        {
        }

        // Nur in dieser Klasse und in Kindklassen sichtbar
        protected void geschuetzteMethode()
        {
        }

        // Diese Methode muss in eine Kindklasse überschrieben werden
        public abstract void abstrakteMethode();

        // Nur diese Methoden können überschrieben werden
        public virtual void ueberschreibbareMethode()
        {
        }

        // schützt eine überschriebene Methode vor weiterem Überschreiben
        public sealed override string ToString()
        {
            return base.ToString();
        }

        // Wenn nichts angegeben wird, dann ist eine Methode private
        void privateMethodeOhneModifier()
        {
        }

        // Nur in der Assembly sichtbar
        internal void interneMethode()
        {
        }
    }

    public class KlasseMitKonstruktor
    {
        // leerer Konstruktor. Dieser wird auch erstellt, wenn man nichts angibt
        public KlasseMitKonstruktor():base()
        {            
        }

        // Kontruktor mit einem Parameter
        public KlasseMitKonstruktor(int number)
        {

        }

        // weitere Konstruktoren müssen eine andere Signatur haben
        public KlasseMitKonstruktor(int number, string beispiel)
        {

        }

        // Ein Konstruktor kann einen anderen Konstruktor aufrufen
        // es wird erst der aufgerufene durchgangen, dann der Konstruktor selbst
        public KlasseMitKonstruktor(int number, int anzahl):this(number)
        {

        }
    }

    public class MethodenBeispiel
    {
        //public void InkrementOut(out int i)
        //{
        //    i = 1;
        //}

        public void Inkrement(ref int i)
        {
            i+=5;
        }
        public void InkrementMulti(ref int i, ref int k)
        {
            i += 5;
        }
    }
}

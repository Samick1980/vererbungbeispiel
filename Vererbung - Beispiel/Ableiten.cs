﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VererbungBeispiel
{
    interface IFahrzeug
    {
        int AnzahlRäder { get; set; }
        void GibGas();
        void blinken();
    }

    abstract class Fahrzeug : IFahrzeug
    {
        private int anzahlRäder;
        public int AnzahlRäder {
            get
            {
                return anzahlRäder;
            }
            set
            {
                anzahlRäder = value;
            }
        }


        public virtual void GibGas()
        {
            Console.WriteLine("GibGas Fahrzeug");
        }

        public void blinken()
        {
            Console.WriteLine("Blinken Fahrzeug");
        }
    }

    class Auto : Fahrzeug
    {
        public Auto()
        {
            AnzahlRäder = 4;
        }

        public new void blinken()
        {
            Console.WriteLine("Blinken Auto");
        }

        public new void GibGas()
        {
            Console.WriteLine("GibGas Auto");
        }
    }

    class Motorrad : Fahrzeug
    {
        new double AnzahlRäder { get; set; }
        public Motorrad()
        {
            AnzahlRäder = 2;
        }

        public override void GibGas()
        {
            Console.WriteLine("GibGas Motorrad");
            //base.GibGas();
        }
    }

    class Schneemobil : IFahrzeug
    {
        public int AnzahlRäder { get; set; }
        public void GibGas()
        {

        }
        public void blinken()
        {

        }       

        public Auto Auto { get; set; }
    }
}
